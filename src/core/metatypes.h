/*
    This file is part of kdepimlibs.

    SPDX-FileCopyrightText: 2010 Will Stephenson <wstephenson@kde.org>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#ifndef METATYPES_H
#define METATYPES_H

#include <QModelIndex>
#include <QMetaType>

Q_DECLARE_METATYPE(QModelIndex)

#endif // METATYPES_H
